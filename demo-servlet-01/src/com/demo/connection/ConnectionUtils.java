/**
 * 
 */
package com.demo.connection;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author YenDauVinh
 *
 */
public class ConnectionUtils {
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL="jdbc:mysql://localhost:3306/SERVLET_APTECH?autoReconnect=true&useSSL=false";
	
	private static final String USER = "root";
	private static final String PASSWORD = "12345678";
	
	private static Connection conn;
	
	public static Connection createConnection() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, USER, PASSWORD);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	public static Connection  getConnection() {
		return conn == null ? createConnection() : conn;
	}
}
