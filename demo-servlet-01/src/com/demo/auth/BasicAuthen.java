/**
 * 
 */
package com.demo.auth;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author NV-Dev
 *
 */
@WebFilter("/*")
public class BasicAuthen implements Filter {

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest  httpReq = (HttpServletRequest)req;
		HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        HttpSession session = request.getSession();
        String url  = httpReq.getRequestURI();
        String loginURI = request.getContextPath() + "/demoServletForm";
        String username = "";
        if(session.getAttribute("username") != null){
        	username = (String) session.getAttribute("username");
        }
        //String username = session.getAttribute("username") != null ? (String) session.getAttribute("username") : "";
        if(url.contains("demoServletForm") || "test".equals(username)){
        	chain.doFilter(request, response);
        }else{
        	request.getRequestDispatcher("loginform.jsp").include(request, response);
        }
	}
}
