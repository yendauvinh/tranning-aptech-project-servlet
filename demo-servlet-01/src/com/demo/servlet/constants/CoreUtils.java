/**
 * 
 */
package com.demo.servlet.constants;

import java.util.HashMap;
import java.util.Map;

/**
 * @author NV-Dev
 *
 */
public class CoreUtils {
	public static Map<String, String> convertToMap(String... arrs){
		if(arrs == null || arrs.length % 2 !=  0) return null;
		Map<String, String> maps = new HashMap<String, String>();
		for(int i = 0 ; i < arrs.length ; i+=2){
			maps.put(arrs[i], arrs[i + 1]);
		}
		return maps;
	} 
}
