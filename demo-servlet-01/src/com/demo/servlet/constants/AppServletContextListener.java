/**
 * 
 */
package com.demo.servlet.constants;

import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @author NV-Dev
 *
 */
@WebListener
public class AppServletContextListener implements ServletContextListener {
	
	private static java.util.Map<String, String> properties;
	
	public void contextInitialized(ServletContextEvent servletContextEvent){
		ServletContext ctx = servletContextEvent.getServletContext();
		Enumeration<String> paramNames = ctx.getInitParameterNames();
		properties = new HashMap<String, String>();
		while(paramNames.hasMoreElements()){
			String paramName = paramNames.nextElement();
			properties.put(paramName , ctx.getInitParameter(paramName));
		}
	}
	
	public static String getInitParam(String paramName){
		return properties.get(paramName) == null ? "" : properties.get(paramName);
	}
}
