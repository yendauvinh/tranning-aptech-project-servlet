/**
 * 
 */
package com.demo.servlet.constants;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

/**
 * @author NV-Dev
 *
 */
public class CookieUtils {
	public static void createCookie(HttpServletResponse response, String name, String value, String... args){
		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(60*60*24);
		if(args != null){
			Map<String, String> configs = CoreUtils.convertToMap(args);
			//: TODO : set properties for cookie: name, time, ...
		}
		response.addCookie(cookie);
	}
	
	public static Map<String, String> getValueFromCookie(Cookie[] cookies){
		if(cookies == null) return null;
		Map<String, String> cookieMap = new HashMap<String, String>();
		for(Cookie cookie : cookies){
			cookieMap.put(cookie.getName(), cookie.getValue());
		}
		return cookieMap;
	}
}
