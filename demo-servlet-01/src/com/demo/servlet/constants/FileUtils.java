/**
 * 
 */
package com.demo.servlet.constants;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.io.IOException;
import java.io.OutputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author NV-Dev
 *
 *
 *TXT
 */
public class FileUtils {
	
	public static void writeFile(String filePath , String fileName, Object... datas ){
		try {
			createFile(filePath, fileName);
			FileWriter fileWriter = new FileWriter(filePath + fileName);
			PrintWriter printWriter = new PrintWriter(fileWriter);
			if(datas != null){
				for(Object data : datas){
					//System.out.println(data.toString());
					printWriter.println(data.toString());
				}
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static List<String> readFile(String filePath){
		List<String> datas = new ArrayList<String>();
		try (Stream<String> stream = Files.lines(Paths.get(filePath))) {
			datas = stream
					.map(String::toUpperCase)
					.collect(Collectors.toList());

		} catch (IOException e) {
			e.printStackTrace();
		}
		return datas;
	}
	
	private static void createFile(String filePath , String fileName){
		Path path = Paths.get(filePath + fileName); //C:\\Directory2\\Sub2\\Sub-Sub2
		if(!Files.exists(path)){
			try {
				Files.createFile(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private static File getFile(String filePath) throws IOException{
		Path path = Paths.get(filePath);
		if(Files.notExists(path)){
			throw new IOException("File is not exist");
		}
		return new File(filePath);
	}
	
	public static void downloadFile(HttpServletRequest request,
            HttpServletResponse response, String filePath){
		try {
			File source = getFile(filePath);
			FileInputStream inputStreamFile = new FileInputStream(source);
			String mimeType = "application/octet-stream";
			response.setContentLength((int) source.length());
			response.setContentType(mimeType);
			String headerKey = "Content-Disposition";
	        String headerValue = String.format("attachment; filename=\"%s\"", source.getName());
	        response.setHeader(headerKey, headerValue);
	        OutputStream outStream = response.getOutputStream();
	        byte[] buffer = new byte[4096];
	        int bytesRead = -1;
	        while ((bytesRead = inputStreamFile.read(buffer)) != -1) {
	            outStream.write(buffer, 0, bytesRead);
	        }
	        inputStreamFile.close();
	        outStream.close(); 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
