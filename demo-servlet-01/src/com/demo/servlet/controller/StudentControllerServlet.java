package com.demo.servlet.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.demo.entity.Student;
import com.demo.service.StudentService;

/**
 * Servlet implementation class StudentControllerServlet
 */
@WebServlet("/StudentControllerServlet")
public class StudentControllerServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	private StudentService studentService = new StudentService();
       
    
    public StudentControllerServlet() {
        super();
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		defineAction(request, response);
	}
	
	public void getAllStudent(HttpServletRequest request , HttpServletResponse response) {
		List<Student> studentList = studentService.getAllStudent();
		request.setAttribute("studentList", studentList);
		
		try {
			request.getRequestDispatcher("student.jsp").include(request, response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}  
	}
	
	private void edit(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		Student student = studentService.getById(id);
		request.setAttribute("student", student);
		request.getRequestDispatcher("editStudent.jsp").forward(request, response);
	}
	
	private void defineAction(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id = request.getParameter("id");
		String action = request.getParameter("action");
		if("delete".equals(action)) {
			delete(id);
		}else {
			if(id != null) {
				edit(request, response);
			}else {
				getAllStudent(request, response);
			}
		}
	}
	
	private void delete(String id) {
		studentService.delete(id);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		Integer age =  Integer.parseInt(request.getParameter("age"));
		Integer id = 0;
		String actionType = request.getParameter("actionType");
		String className = request.getParameter("class");
		Student newStudent = new Student(id, name, age, className, null, null);
		//int id, String name, int age, String className, String sex, String address
		if("UPDATE".equals(actionType)) {
			studentService.update(newStudent);
		}else {
			System.out.println(name + ", " + age + ", " + id);
			//create new id
			int newId = studentService.getMaxId();
			newStudent.setId(newId);
			studentService.insert(newStudent);
		}
		
		getAllStudent(request, response);
	}

}
