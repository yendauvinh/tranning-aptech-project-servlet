package com.demo.servlet.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AysnchonizeServlet
 */
@WebServlet(urlPatterns={"/AysnchonizeServlet"} , asyncSupported = true)
public class AysnchonizeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AysnchonizeServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	threadProcess(request, response);
	}
    
    private void threadProcess(HttpServletRequest request, HttpServletResponse response) throws IOException{
    	response.setContentType("text/html");
    	PrintWriter writer = response.getWriter();
    	writer.println("<html><head><title>" + "Async Servlet</title></head>");
    	writer.println("<body><div id=\"progress\"></div>");
    	final AsyncContext asyncContext = request.startAsync();
    	asyncContext.setTimeout(600*1000);
    	asyncContext.start(new Runnable() {
			@Override
			public void run() {
				for(int i =0 ; i< 100; i++){
					
					writer.print("<script>");
					writer.println("document.getElementById(\"progress\").innerHTML = "
							+  i);
					writer.print("</script>");
					writer.flush();
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				writer.print("<script>");
				writer.println("document.getElementById(\"progress\").innerHTML = "
						+ " \"DONE!\"");
				writer.print("</script>");
				writer.println("</body></html>");
				asyncContext.complete();
			}
		});
    }
    
    private void threadName(HttpServletRequest request, HttpServletResponse response){
    	AsyncContext asyncContext = request.startAsync();
		request.setAttribute("mainThread", Thread.currentThread().getName());
		asyncContext.setTimeout(5000);
		asyncContext.start(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
				}
				request.setAttribute("workerThread", Thread.currentThread().getName());
				asyncContext.dispatch("/threadName.jsp");
			}
		});
    }

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
