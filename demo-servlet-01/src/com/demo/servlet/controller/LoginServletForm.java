package com.demo.servlet.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.demo.servlet.constants.AppServletContextListener;
import com.demo.servlet.constants.CookieUtils;

/**
 * Servlet implementation class DemoServletForm
 */
@WebServlet("/demoServletForm")
public class LoginServletForm extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private String cookieUserName = AppServletContextListener.getInitParam("cookieUserName");
	private String cookiePassword = AppServletContextListener.getInitParam("cookiePassword");
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServletForm() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = getParameter("username" , request);
		String password = getParameter("password" , request);
		if("test".equals(username) && "12345678".equals(password)){
			/*response.sendRedirect("image.jsp");
			response.encodeRedirectURL("image.jsp");
			CookieUtils.createCookie(response, cookieUserName, "test");*/
			//response.sendRedirect("image.jsp"); // không gửi được do sendRedirect gửi request mới sang một page khác
			
			request.getRequestDispatcher("image.jsp").include(request, response);  
			CookieUtils.createCookie(response, cookieUserName, "test");
			CookieUtils.createCookie(response, cookiePassword, "12345678");
			HttpSession session = request.getSession();
			session.setMaxInactiveInterval(1*30);
			session.setAttribute("username", username);
		}else{
			/*request.getRequestDispatcher("errorServlet").forward(request, response);*/
			response.setContentType("text/html");
			response.getWriter().print("sorry, username or password error!");  
            request.getRequestDispatcher("loginform.jsp").include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
	
	private String getParameter(String paramName, HttpServletRequest request) {
		return request.getParameter(paramName);
	}
}
