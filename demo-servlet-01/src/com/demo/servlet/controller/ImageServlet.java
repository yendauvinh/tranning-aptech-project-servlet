package com.demo.servlet.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.demo.servlet.constants.AppServletContextListener;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet(urlPatterns = "/imageServlet" , 
	initParams = {
		@WebInitParam(name = "loginLink", value = "/loginform.jsp"),
		@WebInitParam(name = "username", value = "test"), 
		@WebInitParam(name = "password", value = "12345678")
})
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private String username;
    private String password;
	private String loginLink;
	private String cookieUserName = AppServletContextListener.getInitParam("cookieUserName");
	private String cookiePassword = AppServletContextListener.getInitParam("cookiePassword");
	
	@Override
    public void init(ServletConfig config) throws ServletException {
        /*ServletContext ctx = config.getServletContext();
        cookieUserName = ctx.getInitParameter("cookieUserName");
        cookiePassword = ctx.getInitParameter("cookiePassword");*/
		this.loginLink = config.getInitParameter("loginLink");
		this.username = config.getInitParameter("username");
		this.password = config.getInitParameter("password");
    }
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/*Cookie[] cookies = request.getCookies();
		//boolean requireLogin = verifyLogin(request);
		boolean requireLogin = verifyLoginSession(request);
		System.out.println(requireLogin);
		if(requireLogin) sendToLoginForm(request, response);
		else sendToImage(request, response);*/
		sendToImage(request, response);
	}
	
	public boolean verifyLoginSession(HttpServletRequest request){
		HttpSession session = request.getSession();
		session.setMaxInactiveInterval(10);
		String username = (String)session.getAttribute("username");
		System.out.println("username : " + username);
		if("test".equals(username)){
			return false;
		}else{
			return true;
		}
	}
	
	public boolean verifyLogin(HttpServletRequest request){
		Cookie[] cookies = request.getCookies();
		if(cookies == null || cookies.length == 0) return true;
		String username = "";
		String password = "";
		for(Cookie cookie : cookies){
			if(cookieUserName.equals(cookie.getName())) username = cookie.getValue();
			if(cookiePassword.equals(cookie.getName())) password = cookie.getValue();
		}
		if(!this.username.equals(username) || !this.password.equals(password)){
			return true;
		}
		return false;
	}
	
	public void sendToLoginForm(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException{
		RequestDispatcher requestDispatcher = request.getRequestDispatcher(loginLink);
		requestDispatcher.forward(request, response);
	}
	
	public void sendToImage(HttpServletRequest request , HttpServletResponse response) throws ServletException, IOException{
		/*request.getRequestDispatcher("image.jsp").include(request, response);*/
		response.sendRedirect("image.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}