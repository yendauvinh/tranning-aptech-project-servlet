package com.demo.servlet.controller;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class DemoServlet
 */
@WebServlet("/demoServlet")
public class DemoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DemoServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ServletOutputStream out = response.getOutputStream();
		out.print("<html>");
		out.println("<head><title>Hello Servlet</title></head>");
		out.println("<body>");
		out.println("<h3>Hello World</h3>");
		out.println("This is my first Servlet");
		// :TODO show header infomation.
		String headers = getHeader(request);
		out.println("<div>" + headers + "</div>");
		// :TODO show header infomation.
		String param = getParameter("param1", request);
		out.println("<div>" + param + "</div>");
		// :TODO show header infomation.
		String hostName = getHostName(request);
		out.println("<div>" + hostName + "</div>");
		out.println("<a href=\"/demo-servlet-01/demoServletForm\">Form</a>");
		out.println("</body>");
		out.print("</html>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	private String getParameter(String paramName, HttpServletRequest request) {
		return request.getParameter(paramName);
	}

	private String getHostName(HttpServletRequest request) {
		return request.getServerName();
	}

	private String getHeader(HttpServletRequest request) {
		StringBuffer header = new StringBuffer();
		Enumeration<String> headers = request.getHeaderNames();
		while (headers.hasMoreElements()) {
			String key = headers.nextElement();
			header.append("|| " + key + " : " + request.getHeader(key) + " || ");
		}
		return header.toString();
	}

}
