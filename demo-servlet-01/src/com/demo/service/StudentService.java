package com.demo.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.demo.connection.ConnectionUtils;
import com.demo.entity.Student;

public class StudentService {
	private Connection conn = null;
	
	public List<Student> getAllStudent(){
		List<Student> studentList = new ArrayList<Student>();
		Statement statement = null;
		conn = ConnectionUtils.getConnection();
		try {
			statement = conn.createStatement();
			String queryString = "SELECT ID, AGE, CLASS, NAME FROM STUDENT";
			ResultSet resultSet = statement.executeQuery(queryString);
			while(resultSet.next()) {
				int id  = resultSet.getInt("id");
	            int age = resultSet.getInt("age");
	            String name = resultSet.getString("name");
	            String className = resultSet.getString("class");
	            Student student = new Student(id, name, age, className, null, null);
	            studentList.add(student);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			if(statement != null)
				try {
					statement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
		}
		return studentList;
	}
	
	public int getMaxId() {
		int maxId = 1;
		Statement statement = null;
		conn = ConnectionUtils.getConnection();
		try {
			
			statement = conn.createStatement();
			String queryString = "SELECT MAX(ID) as MAX_ID FROM STUDENT";
			ResultSet resultSet = statement.executeQuery(queryString);
			while(resultSet.next()) {
				maxId = resultSet.getInt("MAX_ID");
				maxId = maxId + 1;
			}
		} catch (Exception e) {
			
		}finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return maxId;
	}
	
	public Boolean insert(Student student) {
		Statement statement = null;
		try {
			conn = ConnectionUtils.getConnection();
			String queryString = "INSERT INTO STUDENT(ID, AGE, CLASS, NAME) VALUES (" 
									+  student.getId() + "," +
									 student.getAge() + ",\'" +
									 student.getClassName() + "\',\'" +
									 student.getName() + "\')" ;
			statement = conn.createStatement();
			statement.executeUpdate(queryString);	
			statement.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
		}
	}
	
	public Boolean update(Student student) {
		PreparedStatement statement = null;
		try {
			conn = ConnectionUtils.getConnection();
			String query = "UPDATE STUDENT SET NAME = ? , AGE = ? , CLASS = ? WHERE ID = ?";
			statement = conn.prepareStatement(query);
			statement.setString(1, student.getName());
			statement.setInt(2, student.getAge());
			statement.setString(3, student.getClassName());
			statement.setInt(4, student.getId());
			statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
		}
	}
	
	public Boolean delete(String id) {
		PreparedStatement statement = null;
		try {
			conn = ConnectionUtils.getConnection();
			String query = "DELETE FROM STUDENT WHERE ID = ?";
			statement = conn.prepareStatement(query);
			statement.setInt(1, Integer.parseInt(id));
			statement.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	public Student getById(String id) {
		Student result = new Student();
		PreparedStatement statement = null;
		conn = ConnectionUtils.getConnection();
		try {
			String queryString = "SELECT ID, AGE, CLASS, NAME FROM STUDENT WHERE ID = ?";
			statement = conn.prepareStatement(queryString);
			statement.setInt(1, Integer.parseInt(id));
			ResultSet resultSet = statement.executeQuery();
			while(resultSet.next()) {
				int idStudent  = resultSet.getInt("id");
	            int age = resultSet.getInt("age");
	            String name = resultSet.getString("name");
	            String className = resultSet.getString("class");
	            result = new Student(idStudent, name, age, className, null, null);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				statement.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	
}
