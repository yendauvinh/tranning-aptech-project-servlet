<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.List"%>
<%@ page import="com.demo.entity.Student"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body>
	<h1>LIST STUDENT</h1>
	<a href="/demo-servlet-01/insertStudent.jsp" class="btn btn-primary" style="margin :10px;"> CREATE </a>
	
	<table class="table table-bordered">
		<tr>
			<th>ID</th>
			<th>NAME</th>
			<th>AGE</th>
			<th>Address</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:choose>
			<c:when test="${not empty studentList}">
				<c:forEach items="${studentList}" var="row">
					<tr>
						<td><c:out value="${row.id}" escapeXml="true"></c:out></td>
						<td><c:out value="${row.name}" escapeXml="true"></c:out></td>
						<td><c:out value="${row.age}" escapeXml="true"></c:out></td>
						<td><c:out value="${row.address}" escapeXml="true"></c:out></td>
						<td><a href="StudentControllerServlet?id=${row.id}" class="btn btn-primary">Edit</a>
						</td>
						<td>
							<a href="StudentControllerServlet?action=delete&id=${row.id}" class="btn btn-danger">Delete</a>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr>
					<td colspan="6">Không tìm thấy học sinh nào!</td>
				</tr>
			</c:otherwise>
		</c:choose>
	</table>
	<!-- SCRIPLET -->
	<%-- <table class="table">
		<tr>
			<td>ID</td>
			<td>NAME</td>
			<td>AGE</td>
			<td>Address</td>
			<td>Update</td>
			<td>Delete</td>
		</tr>
			<% 
				List<Student> studentList = (List<Student>)request.getAttribute("studentList");
				for(Student student : studentList){
					String trStudent = "<tr>";
					trStudent += ("<td>" + student.getId() + "</td>");
					trStudent += ("<td>" + student.getName() + "</td>");
					trStudent += ("<td>" + student.getAge() + "</td>");
					trStudent += ("<td>" + student.getAddress() + "</td>");
					//action update
					trStudent += ("<td> <a href=\"" + "StudentControllerServlet?id=" + student.getId() + " \" >Edit</a></td>");
					//action delete
					trStudent += ("<td> <a href=\"" + "StudentControllerServlet?action=delete&id=" + student.getId() + " \" >Delete</a></td>");
					trStudent += "</tr>";
					out.println(trStudent);
				}
			%>
	</table> --%>
	<!-- SERVLET -->
	<%-- <% 
		List<Student> studentList = (List<Student>)request.getAttribute("studentList");
		for(Student student : studentList){
			String studentInfo = ( "ID is " + student.getId() +
					"Name is " + student.getName() + " " + "Age is " + student.getAge()
					+ " " + "Class is " + student.getClassName());
			out.println(studentInfo);
			out.println("<br/>");
		}
	%> --%>
</body>
</html>