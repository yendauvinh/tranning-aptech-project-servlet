<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="com.demo.entity.Student"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<% Student student = (Student)request.getAttribute("student"); %>
	<h1>Edit Student form</h1>
	
	<form action="StudentControllerServlet" method="post">       
		  Id:  <input name="id" id="id" value="${student.id}" readonly/>
	      Name: <input name="name" id="name" value="${student.name}" placeholder="Enter Name"/>
	      Age:  <input name="age" id="age" value="${student.age}" placeholder="Enter Age"/>
	      Class:  <input name="class" value="${student.className}" id="class" placeholder="Enter Class"/>
	      <input name="actionType" value="UPDATE" style="display: none;"/>
	      <input type="submit" value="Update student"/>
	</form>
</body>
</html>