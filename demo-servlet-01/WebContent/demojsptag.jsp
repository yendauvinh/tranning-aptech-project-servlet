<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>HELLO EVERY BODY</h1>
	Demo jsp expression: The value of 2 raised 4 is <%= Math.pow(2, 4) %>
	<br/>
	
	Example SCRIPTLET : 
	<h2>Directive</h2>
	<%
		String name = "NGUYEN VAN A";
		String className = "C1609L";
		out.println("name is " + name + " , class is " + className);
	%>
	Declaration : 
	<%! int age = 18 ;%>
	<br/>
	<!--  -->
	<%--Scriptlet use here --%>
	Age is  <%= age%>
	
</body>
</html>