<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Insert title here</title>
</head>
<body>
	<div class="container">
		<h2>Request Form</h2>
		<%-- <div class="alert alert-warning" id="msgerr">
			${msgerr}
		</div> --%>
		<form method="GET" action="demoServletForm" class="form-horizontal">
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Username:</label>
			    <div class="col-sm-10">
			      <input name="username" class="form-control w-25" id="username" placeholder="Enter username"/>
			    </div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Password:</label>
			    <div class="col-sm-10">
			      <input name="password" class="form-control w-25" id="password" placeholder="Enter Password"/>
			    </div>
			</div>
			<div class="form-group"> 
			    <div class="col-sm-offset-2 col-sm-10">
			      <button type="submit" class="btn btn-default">Submit</button>
			    </div>
			 </div>
		</form>
	</div>
	<!-- <script type="text/javascript">
		$(document).ready(function(){
		    var msgerr = $("#msgerr").val();
		    if(msgerr == null || msgerr.trim().length == 0){
		    	$("#msgerr").hide();
		    }else{
		    	$("#msgerr").show();
		    }
		});
	</script> -->
</body>
</html>