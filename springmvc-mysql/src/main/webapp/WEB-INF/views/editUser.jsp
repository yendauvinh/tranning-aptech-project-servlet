<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>Update profiles</h1>
	<form:form method="POST" 
				modelAttribute="user" 
				action="${pageContext.request.contextPath}/user/update"
				enctype="multipart/form-data">
		Name : <form:input title="Name" path="name"/><br/>
		Age : <form:input title="Age" path="age"/><br/>
        Select image profile<input type="file" name="file"/><br/>
        <input type="submit" value="Update"/>
	</form:form>
</body>
</html>