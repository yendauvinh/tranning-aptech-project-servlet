<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
</head>
<body>
	<h1>PERSON</h1>
	<form:form action="${pageContext.request.contextPath}/person/add" method="POST" modelAttribute="person">
		Name : <form:input path="name"/><br/>
		Country : <form:input path="country"/><br/>
		<input type="submit" value="Submit"/>
	</form:form>
</body>
</html>