<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<title>Insert title here</title>
</head>
<body>
	<a href="${pageContext.request.contextPath}/person/add">New person</a> <br/>
	
	<h1>LIST PERSON</h1>
	<table>
		<tr>
			<th>ID</th>
			<th>Name</th>
			<th>Country</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${persons}" var="person">
			<tr>
				<td>${person.id}</td>
				<td>${person.name}</td>
				<td>${person.country}</td>
				<td>
					<a href="${pageContext.request.contextPath}/person/update?id=${person.id}">Update</a>
				</td>
				<td>
					<a href="${pageContext.request.contextPath}/person/delete?id=${person.id}">Delete</a>
				</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>