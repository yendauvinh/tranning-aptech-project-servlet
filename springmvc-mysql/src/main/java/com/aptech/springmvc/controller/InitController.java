/**
 * 
 */
package com.aptech.springmvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aptech.springmvc.entity.Person;
import com.aptech.springmvc.entity.User;
import com.aptech.springmvc.service.PersonService;

/**
 * @author YenDauVinh
 *
 */
@Controller
@RequestMapping("/")
public class InitController {
	@Autowired
	private PersonService p;
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value = "/" , method = RequestMethod.GET)
	public ModelAndView init(HttpServletRequest request, HttpServletResponse response) {
		//p.create(new Person());
		ModelAndView model = new ModelAndView("listperson");
		model.addObject("user" , new User());
		model.addObject("test" , "test");
		List<Person> persons = personService.getAll();
		model.addObject("persons", persons);
		return model;
	}
}
