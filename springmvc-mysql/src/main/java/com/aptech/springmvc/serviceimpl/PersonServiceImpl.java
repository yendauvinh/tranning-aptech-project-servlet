/**
 * 
 */
package com.aptech.springmvc.serviceimpl;



import java.util.List;

import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aptech.springmvc.entity.Person;
import com.aptech.springmvc.service.PersonService;

/**
 * @author NV-Dev
 *
 */
@Service
@Transactional
public class PersonServiceImpl implements PersonService {
	/*@Autowired
	private PersonRepository em;*/
	
	@Autowired
	private SessionFactory sessionFactory;

	public Person create(Person person) {
		sessionFactory.getCurrentSession().save(person);
		return person;
	}

	public Person update(Person person) {
		sessionFactory.getCurrentSession().save(person);
		return person;
	}

	public void delete(Person person) {
		sessionFactory.getCurrentSession().delete(person);
	}

	
	@SuppressWarnings("unchecked")
	public List<Person> getAll() {
		List<Person> persons = sessionFactory.getCurrentSession().createQuery("from Person ").list();//hsql
		return persons;
	}
	
	

}
