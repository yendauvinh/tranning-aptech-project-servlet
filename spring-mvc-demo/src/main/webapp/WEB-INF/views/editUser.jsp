<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
	<title>Insert title here</title>
</head>
<body>
	<form:form method="POST" modelAttribute="user" action="${pageContext.request.contextPath}/user/update"
				enctype="multipart/form-data">
		Name : <form:input path="name"/>
		Age : <form:input path="age"/>
		Select image profile<input type="file" name="file"/><br/>
		<input type="submit" value="Login"/>
	</form:form>
</body>
</html>