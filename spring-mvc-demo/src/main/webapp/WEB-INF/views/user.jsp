<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:choose>
		<c:when test="${sessionUser.isLogin}">
		    Name : <h3>${sessionUser.name}</h3>
		    Age : <h3>${user.age}</h3>
			<a href="${pageContext.request.contextPath}/user/getById?id=1">Update profiles</a>
		</c:when>
		<c:otherwise>
			Login failed.
		</c:otherwise>
	</c:choose>
</body>
</html>