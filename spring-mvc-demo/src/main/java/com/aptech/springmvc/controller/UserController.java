/**
 * 
 */
package com.aptech.springmvc.controller;


import java.io.IOException;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.aptech.springmvc.entity.User;
import com.aptech.springmvc.service.UserService;
import com.aptech.springmvc.session.SessionUser;

/**
 * @author NV-Dev
 *
 */
@Controller
@RequestMapping("/user")
@SessionAttributes("sessionUser")
public class UserController {
	@Autowired
	private UserService userServices;
	
	/*
	 * Add user in model attribute
	 * */
	@ModelAttribute("sessionUser")
	public SessionUser setupUserForm() {
		return new SessionUser();
	}
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public ModelAndView  login( @ModelAttribute("user") User user ,
			@ModelAttribute("sessionUser") SessionUser sessionUser) { //@ModelAttribute("user") User user ,
		//ModelAndView mv = new ModelAndView("user");
		ModelAndView mv = new ModelAndView("editUser");
		if(userServices.verifyLogin(user.getUsername(), user.getPassword())) {
			sessionUser.setName(user.getUsername());
			sessionUser.setIsLogin(true);
			sessionUser.setId(1l);
		}else {
			sessionUser.setMessage("Login failed");
		}
		mv.addObject("sessionUser", sessionUser);
		mv.addObject("user" , user);
		return mv;
	}
	
	@RequestMapping(value = "/update" , method = RequestMethod.POST )
	public ModelAndView  update(@ModelAttribute("user") User user ,
			@RequestParam("file") MultipartFile file,
			@SessionAttribute("sessionUser") SessionUser sessionUser
			) throws IOException {
		ModelAndView mv = new ModelAndView("user");
		user.setIsImageProfile(file.getInputStream());
		sessionUser.setName(user.getName());
		mv.addObject("sessionUser", sessionUser);
		mv.addObject("user" , user);
		return mv;
	}
	
	@RequestMapping(value = "/getById" , method = RequestMethod.GET)
	public ModelAndView getById(@RequestParam("id") Long id , 
			@ModelAttribute("sessionUser") SessionUser sessionUser) {
		ModelAndView mv = new ModelAndView("editUser");
		User user = new User();
		if(sessionUser.getId().equals(id)) {
			mv.addObject("sessionUser", sessionUser);
			user.setName(sessionUser.getName());
			user.setId(sessionUser.getId());
			user.setUsername(sessionUser.getName());
		}else {
			mv.addObject("message" , "Errors");
		}
		mv.addObject("user" , user);
		return mv;
	}
	
	
}
