/**
 * 
 */
package com.aptech.springmvc.service;

import com.aptech.springmvc.entity.User;

/**
 * @author NV-Dev
 *
 */
public interface UserService {
	public boolean verifyLogin(String username, String password);
	
	public User update(User user);
}
