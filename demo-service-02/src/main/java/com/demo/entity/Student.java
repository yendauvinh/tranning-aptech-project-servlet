/**
 * 
 */
package com.demo.entity;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author YenDauVinh
 *
 */
@XmlRootElement
public class Student {
	private String name;
	private String code;
	private Integer age;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	public Integer getAge() {
		return age;
	}
	
	public void setAge(Integer age) {
		this.age = age;
	}

	public Student(String name, String code, Integer age) {
		super();
		this.name = name;
		this.code = code;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Student [name=" + name + ", code=" + code + ", age=" + age + "]";
	}

	public Student() {
		super();
	}
}
