/**
 * 
 */
package com.demo.services;

import java.io.IOException;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.demo.entity.Student;

/**
 * @author YenDauVinh
 *
 */
@Path("/hello")
public class ServicesDemo {
	
	@GET
	@Path("/{param}")
	public Response getMsg(@PathParam("param") String msg) {
 
		String output = "Jersey say : " + msg;
		return Response.status(200).entity(output).build();
	}
	
	@POST
	@Path("/addStudent")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces("application/json")
	public Response createStudent(String input) throws JsonParseException, JsonMappingException, IOException {
		//return Response.status(200).entity(student.toString()).build();
		Student student = convertJsonToObject(input);
		//insert db
		return Response.status(200).entity(student.toString()).build();
		
	}
	
	private Student convertJsonToObject(String student) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objMapper = new ObjectMapper();
		Student result = objMapper.readValue(student, Student.class);
		return result;
	}
	
	private String convertObjectToJson(Student student) throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper objMapper = new ObjectMapper();
		String jsonMapping = objMapper.writeValueAsString(student);
		return jsonMapping;
	}
}
