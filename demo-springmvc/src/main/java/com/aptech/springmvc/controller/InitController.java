/**
 * 
 */
package com.aptech.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aptech.springmvc.entity.User;

/**
 * @author YenDauVinh
 *
 */
@Controller
@RequestMapping("/")
public class InitController {
	
	
	@RequestMapping(value = "/" , method = RequestMethod.GET)
	public ModelAndView init(HttpServletRequest request, HttpServletResponse response) {
		ModelAndView model = new ModelAndView("welcome");
		model.addObject("user" , new User());
		model.addObject("test" , "test");
		return model;
	}
}
