/**
 * 
 */
package com.aptech.springmvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.aptech.springmvc.entity.User;
import com.aptech.springmvc.service.UserService;

/**
 * @author NV-Dev
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userServices;
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public ModelAndView  login(HttpServletRequest request, HttpServletResponse response , 
			@ModelAttribute("user") User user) {
		ModelAndView mv = new ModelAndView("editUser");
		Boolean isLogin = null;
		if(userServices.verifyLogin(user.getUsername(), user.getPassword())) {
			isLogin = true;
		}else {
			isLogin = false;
		}
		mv.addObject("isLogin", isLogin);
		return mv;
	}
	
	@RequestMapping(value = "/update" , method = RequestMethod.POST )
	public ModelAndView  update(HttpServletRequest request, HttpServletResponse response , 
			@ModelAttribute("user") User user , @RequestParam("file") MultipartFile file) {
		ModelAndView mv = new ModelAndView("user");
		Boolean isLogin = null;
		if(userServices.verifyLogin(user.getUsername(), user.getPassword())) {
			isLogin = true;
		}else {
			isLogin = false;
		}
		mv.addObject("isLogin", isLogin);
		mv.addObject("user" , user);
		return mv;
	}
}
