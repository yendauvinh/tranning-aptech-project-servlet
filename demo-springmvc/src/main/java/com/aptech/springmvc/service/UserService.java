/**
 * 
 */
package com.aptech.springmvc.service;

/**
 * @author NV-Dev
 *
 */
public interface UserService {
	public boolean verifyLogin(String username, String password);
}
