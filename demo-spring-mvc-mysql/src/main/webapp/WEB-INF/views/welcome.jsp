<%@ page language="java" contentType="text/html; charset=ISO-8859-1"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8" %>

<!DOCTYPE html>
<html>
<head>
	<title>Insert title here</title>
</head>
<body>
	<%-- <h1>Login form</h1>
	<h2>${contextPath}</h2>
	<h2>${test}</h2>
	<form:input path="test"/> --%>
	<h2>${test}</h2>
	<form:form method="POST" modelAttribute="user" action="${pageContext.request.contextPath}/user/login">
		Username : <form:input path="username"/>
		Password : <form:input path="password"/>
		<input type="submit" value="Login"/>
	</form:form>
</body>
</html>