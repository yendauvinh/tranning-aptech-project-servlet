package com.aptech.springmvc.service;

import java.util.List;

import com.aptech.springmvc.entity.Person;

public interface PersonService {
	List<Person> getAll();
	
	Person create(Person person);
	
	Person update(Person person);
	
	void delete(Person person);
}
