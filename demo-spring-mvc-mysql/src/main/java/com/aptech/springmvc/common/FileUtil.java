package com.aptech.springmvc.common;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.FileUtils;

public class FileUtil {
	static final String targetSource = "/source-files";
	
	public static void copyFile(File source) throws IOException {
		File copied = new File(targetSource);
		FileUtils.copyFile(source, copied);
	}
	
	public static void copyFile(InputStream is) throws IOException {
		File copied = new File(targetSource);
		FileUtils.copyInputStreamToFile(is, copied);
	}
}
