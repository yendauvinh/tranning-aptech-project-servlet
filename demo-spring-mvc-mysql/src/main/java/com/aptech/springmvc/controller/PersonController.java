/**
 * 
 */
package com.aptech.springmvc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.aptech.springmvc.entity.Person;
import com.aptech.springmvc.service.PersonService;

/**
 * @author YenDauVinh
 *
 */
@Controller
@RequestMapping(value = "/person")
public class PersonController {
	
	@Autowired
	private PersonService personService;
	
	@RequestMapping(value = "/add" , method = RequestMethod.GET)
	public ModelAndView create() {
		ModelAndView mv = new ModelAndView("person");
		Person p = new Person();
		mv.addObject("person", p);
		return mv;
	}
	
	@RequestMapping(value = "/add" , method = RequestMethod.POST)
	public ModelAndView newPerson(@ModelAttribute("person") Person p) {
		ModelAndView mv = new ModelAndView("listperson");
		personService.create(p);
		List<Person> persons = personService.getAll();
		mv.addObject("persons", persons);
		return mv;
	}
	
	
}
