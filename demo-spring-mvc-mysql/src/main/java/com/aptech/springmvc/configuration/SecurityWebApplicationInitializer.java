/**
 * 
 */
package com.aptech.springmvc.configuration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author YenDauVinh
 *
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer  {
	
}
