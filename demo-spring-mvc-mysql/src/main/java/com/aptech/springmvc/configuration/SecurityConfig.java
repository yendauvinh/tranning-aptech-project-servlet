/**
 * 
 */
package com.aptech.springmvc.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * @author YenDauVinh
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	@Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) 
      throws Exception {
        auth.inMemoryAuthentication().withUser("admin").password("12345678").roles("ADMIN");
    }
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
	    http.authorizeRequests()
	      .antMatchers("/").hasRole("ADMIN")
	      .antMatchers("/admin/**").hasRole("ADMIN")
	      .antMatchers("/**").hasRole("ADMIN")
	      .and()
	      // some more method calls
	      .formLogin()
	      .and().exceptionHandling().accessDeniedPage("/Access_Denied");
	}
}
