/**
 * 
 */
package com.demo.springmvc.controllers;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.demo.springmvc.entity.User;
import com.demo.springmvc.services.UserService;

/**
 * @author root
 *
 */
@Controller
public class LoginController {
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "" , method = RequestMethod.GET)
	public String index() {
		return "index";
	}
	
	@RequestMapping(value = "/login" , method = RequestMethod.POST)
	public String login(Model model , @ModelAttribute User user , HttpServletRequest request, HttpServletResponse response) {
		if(userService.login(user.getUsername(), user.getPassword())) {
			model.addAttribute("username", user.getUsername());
			//save use session.
			HttpSession session =  request.getSession();
			session.setAttribute("username", user.getUsername());
			Cookie userNameCookie = createCookie("username", user.getUsername());
			response.addCookie(userNameCookie);
			return "welcome";
		}else {
			model.addAttribute("message", "You are logged in as ");
			return "index";
		}
	}
	
	private Cookie createCookie(String cookieName, String cookieValue) {
		Cookie cookie = new Cookie(cookieName, cookieValue);
		// Set expiry date after 24 Hrs for both the cookies.
		cookie.setMaxAge(60*60*24);
		return cookie;
	}
}
