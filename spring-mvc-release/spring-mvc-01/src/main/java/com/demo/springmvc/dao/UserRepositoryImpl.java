/**
 * 
 */
package com.demo.springmvc.dao;

import java.util.List;

import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.demo.springmvc.entity.User;

/**
 * @author root
 *
 */
@Repository
public class UserRepositoryImpl implements UserRepository{
	
	@Autowired
	private SessionFactory sessionFactory;

	public List<User> findUserByUsername(String username) {
		Query query = sessionFactory
				.getCurrentSession()
				.createQuery("from User where username = :username");
		query.setParameter("username", username);
		List<User> users = query.list();
		return users;
	}
	
}
