/**
 * 
 */
package com.demo.springmvc.dao;

import java.util.List;

import com.demo.springmvc.entity.User;

/**
 * @author root
 *
 */
public interface UserRepository {
	List<User> findUserByUsername(String username);
}
