/**
 * 
 */
package com.example.demoaop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demoaop.entities.UserAction;
import com.example.demoaop.services.CreatLineServices;
import com.example.demoaop.services.EditLneServices;

/**
 * @author NV-Dev
 *
 */
@RestController
public class FileController {
	@Autowired
	private CreatLineServices creatLineServices;
	@Autowired
	private EditLneServices editLneServices;
	
	@RequestMapping(value = "/file" , method = RequestMethod.GET)
	public String file(@RequestParam("action") String action) {
		if(Integer.parseInt(action) % 2 == 0) {
			//insert.
			UserAction userAction = new UserAction("USER_01", "INSERT");
			creatLineServices.calcualatorSomething(userAction);
		}else {
			//edit.
			UserAction userAction = new UserAction("USER_02", "EDIT");
			editLneServices.calcualatorSomething(userAction);
		}
		return "done";
	}
}
