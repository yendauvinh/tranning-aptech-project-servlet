/**
 * 
 */
package com.example.demoaop.utils;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Service;

import com.example.demoaop.entities.UserAction;

/**
 * @author NV-Dev
 *
 */
@Service
public class FileServices {
	
	private String fileName = "E:/keys/keys.txt";
	
	public void writeFile(String key , UserAction userAction) throws IOException {
		/*		FileWriter fw = new FileWriter(fileName, true);
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write(key);
		bw.newLine();
	    bw.close();*/
		System.out.println("Run");
		try {
			@SuppressWarnings("resource")
			Stream<String> lines = Files.lines(Paths.get(fileName));
			List<String> replaced = lines.collect(Collectors.toList());
			replaced.add(key);
			Files.write(Paths.get(fileName), replaced);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void editFile(String key , UserAction userAction) throws IOException {
		Stream<String> lines = Files.lines(Paths.get(fileName));
		String[] keyAndValue = key.split(" ");
		List<String> replaced = lines.collect(Collectors.toList());
		replaced.remove(Integer.parseInt(keyAndValue[0]));
		replaced.add(Integer.parseInt(keyAndValue[0]), keyAndValue[1]);
		Files.write(Paths.get(fileName), replaced);
	}
}
