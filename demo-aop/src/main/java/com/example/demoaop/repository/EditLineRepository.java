/**
 * 
 */
package com.example.demoaop.repository;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Repository;

/**
 * @author NV-Dev
 *
 */
@Repository
public class EditLineRepository {
	
	public String retrieveSomething() {
		return "0 KEY_" + RandomStringUtils.randomAlphanumeric(10);
	}
	
}
