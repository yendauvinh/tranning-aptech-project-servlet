/**
 * 
 */
package com.example.demoaop.repository;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Repository;

/**
 * @author NV-Dev
 *
 */
@Repository
public class CreateLineRepository {
	
	public String retrieveSomething() {
		return "KEY : " + RandomStringUtils.randomAlphanumeric(10);
	}
	
}
