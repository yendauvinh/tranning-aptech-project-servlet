/**
 * 
 */
package com.example.demoaop.sercurity;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

import com.example.demoaop.entities.UserAction;

/**
 * @author NV-Dev
 *
 */
@Aspect
@Configuration
public class UserAccessAspect {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Before("execution(* com.example.demoaop.services.*.*(..))")
	public void before(JoinPoint joinPoint) throws Exception {
		Object[] args = joinPoint.getArgs();
		UserAction userAction = (UserAction)  args[0];
		logger.info(userAction.getUserName() + "_____________" + userAction.getAction());
	}
}
