/**
 * 
 */
package com.example.demoaop.services;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoaop.entities.UserAction;
import com.example.demoaop.repository.CreateLineRepository;
import com.example.demoaop.utils.FileServices;

/**
 * @author NV-Dev
 *
 */
@Service
public class CreatLineServices {
	
	@Autowired
	private CreateLineRepository businessRepo;
	
	@Autowired
	private FileServices fileService;
	
	public String calcualatorSomething(UserAction userAction) {
		String value =  businessRepo.retrieveSomething();
		try {
			fileService.writeFile(value, userAction);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return value;
	}
}
