/**
 * 
 */
package com.example.demoaop.entities;

/**
 * @author NV-Dev
 *
 */
public class UserAction {
	private String userName;
	private String action;

	public UserAction(String userName , String action) {
		super();
		this.userName = userName;
		this.action = action;
	}

	public UserAction() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
}
