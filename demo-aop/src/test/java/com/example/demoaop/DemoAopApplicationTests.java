package com.example.demoaop;

import java.util.Scanner;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demoaop.entities.UserAction;
import com.example.demoaop.services.CreatLineServices;
import com.example.demoaop.services.EditLneServices;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoAopApplicationTests {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private CreatLineServices creatLineServices;
	@Autowired
	private EditLneServices editLneServices;

	@Test
	public void contextLoads() {
		logger.info("----------begin------------------");
		//logger.info(businessService.calcualatorSomething());
		while(true) {
			Scanner sc = new Scanner(System.in);
			System.out.println("Nhap so");
			int action = sc.nextInt();
			if(action % 2 == 0) {
				//insert.
				UserAction userAction = new UserAction("USER_01", "INSERT");
				creatLineServices.calcualatorSomething(userAction);
			}else {
				//edit.
				UserAction userAction = new UserAction("USER_02", "EDIT");
				editLneServices.calcualatorSomething(userAction);
			}
			System.out.println("HIHIHIHIHI");
		}
		
	}

}

